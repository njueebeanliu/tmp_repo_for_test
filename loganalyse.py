#!/usr/bin/env python
import sys
import re
file = sys.argv[1]
player_win_times = {}
for i in range(1,9):
    a = str(i)
    player_win_times[a+a+a+a] = 0
with open(file,'r') as f:
    list_f = list(enumerate(f))
    for line in list_f:
        match_over = re.match(r'game-over',line[1])
        if match_over:
            inquire_head = line[0]
            while not re.match(r'inquire/',list_f[inquire_head][1]):
                inquire_head-= 1
            inquire_end = inquire_head
            while not re.match(r'/inquire',list_f[inquire_end][1]):
                inquire_end +=1
                

            player_remained = {}
            for index in range(inquire_head+1,inquire_end-1):
                imfo = re.split(r'\s+',list_f[index][1]) 
                player_id = imfo[0]
                player_jetton = int(imfo[1])
                player_money = int(imfo[2])
                player_remained[player_id] = player_money + player_jetton
            pot_win_line = inquire_end
            while not re.match(r'pot-win/',list_f[pot_win_line][1]):
                pot_win_line += 1
            pot_win = re.split(r'\D+',list_f[pot_win_line+1][1])
            if pot_win[0]:
                player_remained[pot_win[0]] += int(pot_win[1])
            sorted_player = sorted(player_remained.items(), key=lambda player_remained:player_remained[1],reverse = True)
            print sorted_player
            player_win_times[sorted_player[0][0]] += 1
print player_win_times
sorted_player_win = sorted(player_win_times.items(), key=lambda player_win_times:player_win_times[1],reverse = True)
print sorted_player_win


